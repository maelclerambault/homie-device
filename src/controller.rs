use super::Error;
use std::sync::mpsc::Receiver;
use std::sync::Arc;
use super::{State, Datatype};


#[derive(Debug)]
pub enum Attribute {
    HomieVersion(String),
    Name(String),
    State(State),
    DataType(Datatype),
    Nodes(Vec<String>),
    Properties(Vec<String>),
    Retained(bool),
    Settable(bool),
    Other(String, String),
}

#[derive(Debug)]
pub enum Event {
    DeviceAttribute(String, Attribute),
    NodeAttribute(String, String, Attribute),
    PropertyAttribute(String, String, String, Attribute),
    PropertyValue(String, String, String, String),
}


fn strip_homie_prefix<'a, 'b>(topic: &'a str, prefix: &'b str) -> Option<&'a str> {
    if topic.starts_with(prefix) {
        Some(&topic[prefix.len() .. ])
    } else {
        None
    }
}

pub struct Controller {
    mqtt: Arc<paho_mqtt::Client>,
    prefix: String,
    receiver: Receiver<Option<paho_mqtt::Message>>
}

impl Controller {
    pub fn new(app_id: &str, host: &str) -> Result<Controller, Error> {
        let prefix = "homie";
        let mut mqtt = paho_mqtt::Client::new(
            paho_mqtt::CreateOptionsBuilder::new()
                .server_uri(host)
                .client_id(app_id)
                .persistence(paho_mqtt::PersistenceType::None)
                .finalize()
        )?;
        
        mqtt.connect(None)?;
        let receiver = mqtt.start_consuming();
        mqtt.subscribe(&(prefix.to_string() + "/#"), 0)?;

        Ok(Controller {
            mqtt: Arc::new(mqtt),
            prefix: prefix.to_string(),
            receiver
        })
    }
    
    fn parse_message<'a, 'b>(&mut self, topic: &'b str, payload: &'a str) -> Result<Event, Error> {
        let topic = strip_homie_prefix(topic, &(self.prefix.clone() + "/")).unwrap();

        Ok(match topic.split("/").collect::<Vec<_>>().as_slice() {
            [device_id, "$homie"] => Event::DeviceAttribute(
                device_id.to_string(),
                Attribute::HomieVersion(payload.to_string())
            ),
            [device_id, "$name"] => Event::DeviceAttribute(
                device_id.to_string(),
                Attribute::Name(payload.to_string())
            ),
            [device_id, "$state"] => Event::DeviceAttribute(
                device_id.to_string(),
                Attribute::State(payload.parse()?)
            ),
            [device_id, "$nodes"] => Event::DeviceAttribute(
                device_id.to_string(),
                Attribute::Nodes(payload.split(",").map(|s| s.to_string()).collect())
            ),

            [device_id, attribute] if attribute.starts_with("$") => Event::DeviceAttribute(
                device_id.to_string(),
                Attribute::Other(attribute.to_string(), payload.to_string())
            ),
            [device_id, node_id, "$name"] => Event::NodeAttribute(
                device_id.to_string(),
                node_id.to_string(),
                Attribute::Name(payload.to_string())
            ),            
            [device_id, node_id, "$properties"] => Event::NodeAttribute(
                device_id.to_string(),
                node_id.to_string(),
                Attribute::Properties(payload.split(",").map(|s| s.to_string()).collect())
            ),
            [device_id, node_id, attribute] if attribute.starts_with("$") => Event::NodeAttribute(
                device_id.to_string(),
                node_id.to_string(),
                Attribute::Other(attribute.to_string(), payload.to_string())
            ),
            [device_id, node_id, property_id, "$datatype"] => Event::PropertyAttribute(
                device_id.to_string(),
                node_id.to_string(),
                property_id.to_string(),
                Attribute::DataType(payload.parse()?)
            ),
            [device_id, node_id, property_id, "$retained"] => Event::PropertyAttribute(
                device_id.to_string(),
                node_id.to_string(),
                property_id.to_string(),
                Attribute::Retained(payload.parse()?)
            ),
            [device_id, node_id, property_id, "$settable"] => Event::PropertyAttribute(
                device_id.to_string(),
                node_id.to_string(),
                property_id.to_string(),
                Attribute::Settable(payload.parse()?)
            ),
            [device_id, node_id, property_id, "$name"] => Event::PropertyAttribute(
                device_id.to_string(),
                node_id.to_string(),
                property_id.to_string(),
                Attribute::Name(payload.to_string())
            ),
            [device_id, node_id, property_id, attribute] if attribute.starts_with("$") => Event::PropertyAttribute(
                device_id.to_string(),
                node_id.to_string(),
                property_id.to_string(),
                Attribute::Other(attribute.to_string(), payload.to_string())
            ),
            [device_id, node_id, property_id] => Event::PropertyValue(
                device_id.to_string(),
                node_id.to_string(),
                property_id.to_string(),
                payload.to_string()
            ),

            unknown_topic @ _ => panic!("unknown topic {}", unknown_topic.join("/"))
        })
    }
    
    pub fn iter(&mut self) -> Properties {
        Properties(self)
    }
}

pub struct Properties<'a>(&'a mut Controller);

impl<'a> Iterator for Properties<'a> {
    type Item = Event;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.receiver.recv().unwrap().map(|message| {
            let topic = message.topic();
            let payload = message.payload_str();

            self.0.parse_message(topic, &payload).unwrap()
        })
    }
}

