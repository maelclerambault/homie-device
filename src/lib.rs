//! [Homie](https://homieiot.github.io) implementation

use std::sync::Arc;
use std::sync::mpsc::Receiver;
use thiserror::Error;

pub mod controller;

#[derive(Error, Debug)]
pub enum Error {
    #[error("mqtt")]
    Mqtt(#[from] paho_mqtt::Error),
    
    #[error("state")]
    StateParse,
    
    #[error("datatype")]
    DataTypeParse,
    
    #[error("parse error")]
    BoolParse(#[from] std::str::ParseBoolError),
}

struct Client {
    mqtt: paho_mqtt::Client,
}

impl Client {
    fn new(client_id: Option<String>, host: &str) -> Result<(Arc<Client>, Receiver<Option<paho_mqtt::Message>>), Error> {
        use paho_mqtt::CreateOptionsBuilder;

        let mut create_options_builder = CreateOptionsBuilder::new();
        if let Some(client_id) = client_id {
            create_options_builder = create_options_builder.client_id(client_id);
        }
        let mut mqtt = paho_mqtt::Client::new(
            create_options_builder
                .server_uri(host)
                .persistence(paho_mqtt::PersistenceType::None)
                .finalize()
        )?;
        let receiver = mqtt.start_consuming();
        
        Ok(
            (Arc::new(Client {mqtt}), receiver)
        )
    }
    
    fn subscribe(&self, topic_name: &str) -> Result<(), Error> {
        self.mqtt.subscribe(topic_name, 0)?;
        Ok(())
    }
    
    fn publish<V: Into<Vec<u8>>>(&self, topic: &str, value: V, retained: bool) -> Result<(), Error> {
        use paho_mqtt::MessageBuilder;
        
        self.mqtt.publish(
            MessageBuilder::new()
                .topic(topic)
                .payload(value)
                .retained(retained)
                .finalize()
        )?;
        Ok(())
    }
}

#[derive(Clone)]
struct TopicPublisher {
    topic: String,
    client: Arc<Client>
}

impl TopicPublisher {
    fn publish<V: Into<Vec<u8>>>(&self, value: V, retained: bool) -> Result<(), Error> {
        self.client.publish(&self.topic, value, retained)
    }
    
    fn subscribe(&self) -> Result<(), Error> {
        self.client.subscribe(&self.topic)
    }
    
    fn append(&self, subtopic: &str) -> TopicPublisher {
         TopicPublisher {
            topic: self.topic.clone() + "/" + subtopic,
            client: self.client.clone(),
        }
    }
}

#[derive(Debug)]
pub enum Datatype {Integer, Float, Boolean, String, Enum, Color}

impl std::fmt::Display for Datatype {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let s = match self {
            Datatype::Integer => "integer",
            Datatype::Float => "float",
            Datatype::Boolean => "boolean",
            Datatype::String => "string",
            Datatype::Enum => "enum",
            Datatype::Color => "color",
            
        };
        write!(f, "{}", s)
    }
}

impl std::str::FromStr for Datatype {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
           "integer" => Ok(Datatype::Integer),
           "float" => Ok(Datatype::Float),
           "boolean" => Ok(Datatype::Boolean),
           "string" => Ok(Datatype::String),
           "enum" => Ok(Datatype::Enum),
           "color" => Ok(Datatype::Color),
            _       => Err(Error::DataTypeParse)
        }
    }
}

/// Helper trait for setting properties `$datatype`
pub trait DatatypeTrait {
    fn datatype() -> Datatype;
}

/// A typed property
/// 
/// Use this object to update the state of the property
#[derive(Clone)]
pub struct Property<T> {
    topic: TopicPublisher,
    topic_set: String,
    retained: bool,
    _marker: std::marker::PhantomData<T>
}

impl DatatypeTrait for Property<bool> {
    fn datatype() -> Datatype {
        Datatype::Boolean
    }
}

impl DatatypeTrait for Property<u32> {
    fn datatype() -> Datatype {
        Datatype::Integer
    }
}

impl DatatypeTrait for Property<u64> {
    fn datatype() -> Datatype {
        Datatype::Integer
    }
}

impl DatatypeTrait for Property<i32> {
    fn datatype() -> Datatype {
        Datatype::Integer
    }
}

impl DatatypeTrait for Property<i64> {
    fn datatype() -> Datatype {
        Datatype::Integer
    }
}

impl DatatypeTrait for Property<f32> {
    fn datatype() -> Datatype {
        Datatype::Float
    }
}

impl DatatypeTrait for Property<f64> {
    fn datatype() -> Datatype {
        Datatype::Float
    }
}

impl DatatypeTrait for Property<String> {
    fn datatype() -> Datatype {
        Datatype::String
    }
}

impl<'a> DatatypeTrait for Property<&'a str> {
    fn datatype() -> Datatype {
        Datatype::String
    }
}

impl<T> Property<T> {
    /// Property set topic
    ///
    /// eg: homie/device-id/node-id/property-id/set
    pub fn topic_set(&self) -> &str {
        &self.topic_set
    }
    
    /// Property topic
    ///
    /// eg: homie/device-id/node-id/property-id
    pub fn topic(&self) -> &str {
        &self.topic.topic
    }
}

impl<T: ToString> Property<T> {
    /// Set the state of a property
    ///
    /// # Usage
    ///
    /// ```
    /// prop1.set_payload("state");
    /// prop2.set_payload(true);
    /// prop3.set_payload(12345.6);
    /// ```
    pub fn set_payload(&self, value: T) -> Result<(), Error> {
        self.topic.publish(value.to_string(), self.retained)
    }
}

impl<T: std::str::FromStr> Property<T> {
    /// Read and parse the mqtt payload
    pub fn read_payload(&self, payload: &str) -> Result<T, T::Err> {
        payload.parse()
    }
}

pub mod configuration {
    use super::*;
    
    /// Configure a Property
    pub struct PropertyConfiguration {
        id: String,
        name: String,
        datatype: String,
        topic: TopicPublisher,
        
        /// Non-retained (false). Default is Retained (true).
        pub retained: bool,

        /// Specifies restrictions or options for the given data type
        pub format: Option<String>,
        
        /// Settable (true). Default is read-only (false)
        pub settable: bool,
        
        /// Optional unit of this property. See list below.
        pub unit: Option<String>,
    }

    impl PropertyConfiguration {
        fn publish(&self) -> Result<(), Error> {
            // Required attributes
            self.topic.append("$name").publish(self.name.as_bytes(), true)?;
            self.topic.append("$datatype").publish(self.datatype.as_bytes(), true)?;

            // Optional attributes
            if let Some(format) = &self.format {
                self.topic.append("$format").publish(format.as_bytes(), true)?;
            }
            if self.settable {
                self.topic.append("set").subscribe()?;
                self.topic.append("$settable").publish("true", true)?;
            }
            if !self.retained {
                self.topic.append("$retained").publish("false", true)?;
            }
            if let Some(unit) = &self.unit {
                self.topic.append("$unit").publish(unit.as_bytes(), true)?;
            }
            
            Ok(())
        }
    }

    /// Configure a Node
    pub struct NodeConfiguration {
        id: String,
        name: String,
        r#type: String,
        topic: TopicPublisher,
        properties: Vec<PropertyConfiguration>
    }

    impl NodeConfiguration {
        pub fn add_property<F, T>(&mut self, id: &str, name: &str, f: F) -> Property<T>
            where F: FnOnce(&mut PropertyConfiguration),
                Property<T> : DatatypeTrait
        {
            
            let mut property_configuration = PropertyConfiguration {
                id: id.to_string(),
                name: name.to_string(),
                datatype: Property::<T>::datatype().to_string(),
                topic: self.topic.append(id),
                retained: true,
                settable: false,
                format: None,
                unit: None
            };
            let property = Property {
                topic: property_configuration.topic.clone(),
                topic_set: property_configuration.topic.append("set").topic,
                retained: property_configuration.retained,
                _marker: std::marker::PhantomData
            };
            
            f(&mut property_configuration);
            self.properties.push(property_configuration);
            
            property
        }
        
        fn publish(&self) -> Result<(), Error> {
            for property in &self.properties {
                property.publish()?;
            }
            self.topic.append("$name").publish(self.name.as_bytes(), true)?;
            self.topic.append("$type").publish(self.r#type.as_bytes(), true)?;
            self.topic.append("$properties").publish(
                self.properties
                    .iter().map(|property| property.id.as_str())
                    .collect::<Vec<_>>()
                    .join(","),
                true)?;
            
            Ok(())
        }
    }


    /// Configure a homie device with its nodes and properties
    pub struct DeviceConfiguration {
        name: String,
        topic: TopicPublisher,
        nodes: Vec<NodeConfiguration>
    }

    impl DeviceConfiguration {
        pub(super) fn new(root_topic: TopicPublisher, name: &str) -> DeviceConfiguration {
        DeviceConfiguration {
                name: name.to_string(),
                topic: root_topic,
                nodes: vec![]
        }
        }
        
        pub(super) fn publish(&self) -> Result<(), Error> {
            for node in &self.nodes {
                node.publish()?;
            }
            self.topic.append("$homie").publish("4.0.0", true)?;
            self.topic.append("$name").publish(self.name.as_bytes(), true)?;
            self.topic.append("$nodes").publish(
                self.nodes
                    .iter().map(|node| node.id.as_str())
                    .collect::<Vec<_>>()
                    .join(","),
                true)?;
            self.topic.append("$extensions").publish("", true)?;
            
            Ok(())
        }
        
        /// Add a node
        ///
        /// Returns the result of the provided function
        ///
        pub fn add_node<F, R>(&mut self, id: &str, name: &str, r#type: &str, f: F) -> R
            where F: FnOnce(&mut NodeConfiguration) -> R
        {
            let mut node = NodeConfiguration {
                id: id.to_string(),
                name: name.to_string(),
                r#type: r#type.to_string(),
                topic: self.topic.append(id),
                properties: vec![]
            };

            let r = f(&mut node);
            self.nodes.push(node);

            r
        }
    }
}

/// Device Lifecycle
#[derive(Debug)]
pub enum State {
    /// This is the state the device is in when it is connected to the MQTT 
    /// broker, but has not yet sent all Homie messages and is not yet ready to operate.
    /// 
    /// This state is optional, and may be sent if the device takes a long time to 
    /// initialize, but wishes to announce to consumers that it is coming online.
    Init,
    
    /// This is the state the device is in when it is connected to the MQTT 
    /// broker, has sent all Homie messages and is ready to operate. A Homie Controller 
    /// can assume default values for all optional topics.
    Ready,
    
    /// This is the state the device is in when it is cleanly disconnected from 
    /// the MQTT broker. You must send this message before cleanly disconnecting. 
    Disconnected,
    
    /// This is the state the device is in when the device is sleeping. You have 
    /// to send this message before sleeping.
    Sleeping,
    
    /// This is the state the device is in when the device has been "badly"
    /// disconnected. You must define this message as LWT.
    Lost,
    
    /// This is the state the device is when connected to the MQTT broker, but 
    /// something wrong is happening. E.g. a sensor is not providing data and needs 
    /// human intervention. You have to send this message when something is wrong.
    Alert
}

impl std::fmt::Display for State {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let s = match self {
            State::Init => "init",
            State::Ready => "ready",
            State::Disconnected => "disconnected",
            State::Sleeping => "sleeping",
            State::Lost => "lost",
            State::Alert => "alert"
        };
        write!(f, "{}", s)
    }
}

impl std::str::FromStr for State {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "init" => Ok(State::Init),
            "ready" => Ok(State::Ready),
            "disconnected" => Ok(State::Disconnected),
            "sleeping" => Ok(State::Sleeping),
            "lost" => Ok(State::Lost),
            "alert" => Ok(State::Alert),
            _       => Err(Error::StateParse)
        }
    }
}


/// Homie connection authentication
enum DeviceAuth {
    None,
    Tls(std::path::PathBuf, std::path::PathBuf)
}

/// Homie connection configuration
pub struct DeviceConfiguration {
    host: String,
    client_id: Option<String>,
    prefix: String,
    id: String,
    auth: DeviceAuth,
}

impl DeviceConfiguration {
    pub fn set_client_id(mut self, client_id: &str) -> Self {
        self.client_id = Some(client_id.to_string());
        self
    }
    
    pub fn set_prefix(mut self, prefix: &str) -> Self {
        self.prefix = prefix.to_string();
        self
    }
    
    pub fn set_tls<P>(mut self, cert: P, key: P) -> Self
        where P: AsRef<std::path::Path>
    {
        self.auth = DeviceAuth::Tls(cert.as_ref().to_owned(), key.as_ref().to_owned());
        self
    }
}

/// Homie connection and state management
pub struct Device {
    topic: TopicPublisher,
    receiver: Receiver<Option<paho_mqtt::Message>>
}

impl Device {
    /// Create a homie device connection
    pub fn new<F>(mqtt_host: &str, id: &str, f: F) -> Result<Device, Error>
        where F: FnOnce(DeviceConfiguration) -> DeviceConfiguration
    {
        use paho_mqtt::{ConnectOptionsBuilder, SslOptionsBuilder, SslVersion, Message};
        
        let device_configuration = f(DeviceConfiguration {
            host: mqtt_host.to_string(),
            client_id: None,
            prefix: "homie".to_string(),
            id: id.to_string(),
            auth: DeviceAuth::None,
        });

        let topic = device_configuration.prefix.to_string() + "/" + &device_configuration.id;
        let state_topic = topic.clone() + "/$state";
        let (client, receiver) = Client::new(device_configuration.client_id, &device_configuration.host)?;
        let mut connect_option_builder = ConnectOptionsBuilder::new();
        if let DeviceAuth::Tls(cert, key) = device_configuration.auth {
            connect_option_builder.ssl_options(
                SslOptionsBuilder::new()
                    .key_store(cert).unwrap()
                    .private_key(key).unwrap()
                    .enable_server_cert_auth(false)
                    .finalize()
            );
        }
        
        client.mqtt.connect(
            connect_option_builder
                .will_message(Message::new_retained(&state_topic, State::Lost.to_string(), 0))
                .finalize()
        )?;
        let topic = TopicPublisher {topic, client};
        
        Ok(Device {topic, receiver})
    }

    /// Publish all device/nodes/properties attributes to the relevant topics
    ///
    /// Returns the result of the provided function.
    ///
    /// The device `$state` is set to `ready`
    pub fn configure<F, T>(&mut self, name: &str, f: F) -> Result<T, Error>
        where F: FnOnce(&mut configuration::DeviceConfiguration) -> T
    {
        let mut configuration = configuration::DeviceConfiguration::new(self.topic.clone(), name);
        let r = f(&mut configuration);
        
        configuration.publish()?;
        self.set_state(State::Ready)?;

        Ok(r)
    }

    pub fn is_connected(&self) -> bool {
        self.topic.client.mqtt.is_connected()
    }
    
    pub fn set_state(&mut self, state: State) -> Result<(), Error>{
        self.topic.append("$state").publish(state.to_string(), true)
    }
    
    /// Blocking loop through messages
    ///
    /// ```
    /// for (topic, payload) in device.messages() {
    ///     if topic == property.topic_set() {
    ///         let new_state = switch.read_payload(&payload).unwrap();
    ///         switch.set_payload(new_state).unwrap();
    ///         // ...
    ///     }
    /// }
    /// ```
    pub fn messages(&mut self) -> Messages {
        Messages(&self.receiver)
    }
}

pub struct Messages<'a>(&'a std::sync::mpsc::Receiver<Option<paho_mqtt::Message>>);

impl<'a> Iterator for Messages<'a> {
    type Item = (String, String);

    fn next(&mut self) -> Option<Self::Item> {
        self.0.recv().unwrap().map(|message|
            (message.topic().to_string(), message.payload_str().into_owned())
        )
    }
}

