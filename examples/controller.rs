const CLIENT_ID: &str = "controller";
const BROKER: &str = "192.168.6.43:1883";

fn main() {
    let mut controller = homie_device::controller::Controller::new(CLIENT_ID, BROKER).unwrap();
    
    for message in controller.iter() {
        dbg!(message);
    }
}
