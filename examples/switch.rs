const CLIENT_ID: &str = "switch-device";
const BROKER: &str = "localhost:1883";

// Test with 
//
// `mosquitto_pub -t homie/switch-device/switch/value/set -m true` (or false)
//
// Monitor state and attributes with
//
// mosquitto_sub -v -t "homie/switch-device/#"
fn main() {
    let mut device = homie_device::Device::new(CLIENT_ID, BROKER, "homie", "switch-device").unwrap();
    let switch = device.configure("Switch Device", |device| {
        device.add_node("switch", "Switch", "binary", |node| {
            node.add_property("value", "Value", |property| property.settable = true)
        })
    }).unwrap();
    
    switch.set_payload(false).unwrap();
    
    for (topic, payload) in device.messages() {
        if topic == switch.topic_set() {
            let new_state = switch.read_payload(&payload).unwrap();
            switch.set_payload(new_state).unwrap();
            dbg!(new_state);
        }
    }
}
