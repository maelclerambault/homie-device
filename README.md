# homie-device

[Homie](https://homieiot.github.io) implementation in rust.

## Usage

```rust
let mut device = homie_device::Device::new("client-id", "localhost:1883", "homie", "switch-device").unwrap();
let mut (prop1, prop2) = device.configure("Device", |device| {
    device.add_node("node1", "Node 1", "nodeType", |node| {
        let prop1 = node.add_property("prop1", "Property 1", |_| {});
        let prop2 = node.add_property("prop2", "Property 2", |_| {});
        (prop1, prop2)
    })
}).unwrap();

prop1.set_payload("state").unwrap();
```
